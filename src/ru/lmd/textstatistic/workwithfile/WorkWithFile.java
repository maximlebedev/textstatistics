package ru.lmd.textstatistic.workwithfile;

import java.io.*;

/**
 * Класс для работы с файлами
 *
 * @author M. Lebedev 17IT18
 */
public class WorkWithFile {

    /**
     * Метод считывает данные с файла
     *
     * @param inputWay путь до файла
     * @return входные данные
     * @throws IOException исключение связанное чтением/записью
     */
    public static String readFile(String inputWay) throws IOException {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(inputWay));
            String data;
            data = bufferedReader.readLine();
            return data;
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException();
        } catch (IOException e) {
            throw new IOException();
        }
    }

    /**
     * Метод записывает данные в файл
     *
     * @param outputWay              путь до файла
     * @param space                  кол-во пробелов
     * @param charactersWithoutSpace кол-во символов без пробелов
     * @param characters             кол-во символов
     * @param words                  кол-во слов
     * @throws IOException исключение связанное чтением/записью
     */
    public static void writeFile(String outputWay, int space, int charactersWithoutSpace, int characters, int words) throws IOException {
        try (FileWriter writer = new FileWriter(outputWay, false)) {
            writer.write("Кол-во пробелов в тексте - " + space + "\n");
            writer.write("Кол-во символов в тексте без пробела - " + charactersWithoutSpace + "\n");
            writer.write("Кол-во симоволов в тексте - " + characters + "\n");
            writer.write("Кол-во слов в тексте - " + words + "\n");
            writer.flush();
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException();
        } catch (IOException e) {
            throw new IOException();
        }
    }
}
