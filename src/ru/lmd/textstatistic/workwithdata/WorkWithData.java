package ru.lmd.textstatistic.workwithdata;

import java.util.regex.Pattern;

/**
 * Класс для работы с входными данными
 *
 * @author M. Lebedev 17IT18
 */
public class WorkWithData {

    /**
     * Метод считает кол-во пробелы
     *
     * @param data входные данные
     * @return кол-во пробелов
     */
    public static int findSpace(String data) {
        int space = 0;
        String[] dataArray = data.split("");

        for (int i = 0; i < dataArray.length; i++) {
            if (dataArray[i].matches("\\s")) {
                space++;
            }
        }

        return space;
    }

    /**
     * Метод считает кол-во символы
     *
     * @param data входные данные
     * @return кол-во символов
     */
    public static int findCharacters(String data) {
        String[] dataArray = data.split("");
        int characters = dataArray.length;

        return characters;
    }

    /**
     * Метод считает кол-во символов без пробелов
     *
     * @param characters кол-во символов
     * @param space кол-во пробелов
     * @return кол-во символов без пробелов
     */
    public static int charactersWithoutSpace(int characters, int space) {
        int charactersWithoutSpace = characters - space;

        return charactersWithoutSpace;
    }

    /**
     * Метод считает кол-во слов в тексте
     *
     * @param data входные данные
     * @return кол-во слов
     */
    public static int findWords(String data) {
        int words = 0;

        String[] dataArray = data.split(" ");

        for (int i = 0; i < dataArray.length; i++) {
            if (!dataArray[i].matches("[\\s]+")) {
                words++;
            }

            if(dataArray[i].equals("")) {
                words--;
            }

            if(dataArray[i].matches("[,.\\-:;\'\"?]+")) {
                words--;
            }
        }
        return words;
    }
}
