package ru.lmd.textstatistic;

import ru.lmd.textstatistic.workwithdata.WorkWithData;
import ru.lmd.textstatistic.workwithfile.WorkWithFile;
import ru.lmd.textstatistic.workwithconsole.WorkWithConsole;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Класс для работы с другими классами, и их методами
 *
 * @author M. Lebedev 17IT18
 */
public class Main {

    public static void main(String[] args) {
        String inputWay = "src\\ru\\lmd\\textstatistic\\file\\input.txt";
        String outputWay = "src\\ru\\lmd\\textstatistic\\file\\output.txt";

        try {
            String data = WorkWithFile.readFile(inputWay);

            int space = WorkWithData.findSpace(data);
            int characters = WorkWithData.findCharacters(data);
            int charactersWithoutSpace = WorkWithData.charactersWithoutSpace(characters, space);
            int words = WorkWithData.findWords(data);

            WorkWithConsole.outputDataInConsole(space, charactersWithoutSpace, characters, words);
            WorkWithFile.writeFile(outputWay, space, charactersWithoutSpace, characters, words);

        } catch (FileNotFoundException e) {
            System.err.println("File not found!");
        } catch (IOException e) {
            System.err.println("Input/Output Exception!");
        }
    }
}
