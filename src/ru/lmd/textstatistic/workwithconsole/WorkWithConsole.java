package ru.lmd.textstatistic.workwithconsole;

/**
 * Класс для работы с консолью
 *
 * @author M. Lebedev 17IT18
 */
public class WorkWithConsole {

    /**
     * Метод выводит данные в консоль
     *
     * @param space                  кол-во пробелов
     * @param charactersWithoutSpace кол-во символов без пробелов
     * @param characters             кол-во символов
     * @param words                  кол-во слов
     */
    public static void outputDataInConsole(int space, int charactersWithoutSpace, int characters, int words) {
        System.out.println("Кол-во пробелов в тексте - " + space);
        System.out.println("Кол-во символов в тексте без пробелов - " + charactersWithoutSpace);
        System.out.println("Кол-во символов в тексте - " + characters);
        System.out.println("Кол-во слов в тексте - " + words);
    }
}
